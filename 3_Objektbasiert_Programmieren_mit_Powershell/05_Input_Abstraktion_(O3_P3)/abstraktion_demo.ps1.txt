﻿# Abstraktionen: Cmdlet, Standard-Funktionen aus der .NET-Bibliothek, Objekt-Methoden
# Beispiele: 
# get-process P*
# [Math]::round(0.545,2)
# (get-process notepad).kill()





#Es werden nun drei versch. Arten gezeigt wie man eine Funktion definieren kann.
#Danach werden die vier demo-Funktionen mit unterschiedlichen Parameteranordnungen aufgerufen.
#Demo: Die drei Funktionen jeweils nacheinander aufbauen und verschieden aufrufen...


#Abstraktion ohne Parameter
function demo0 
{
  
   return "Demo 0 " * 5 #Rückgabewert wird hier erzeugt und zurückgegeben
 
}

#Abstraktion mit typisierten Parameter (Reihenfolge zwingend)
function demo1 ([int]$zahl, [string]$text)
{
   $resultat = $text * $zahl  #Rückgabewert wird hier erzeugt
   return $resultat           #Rückgabewert wird hier zurückgegeben
 
}

#Abstraktion mit benannten Parameter (Reihenfolge zwingend oder benannte Parameter verwenden)
function demo2 
{
   Param ([int]$zahl, [string]$text) #Parameter werden benannt und typisiert
   return $text * $zahl
 
}


#Abstraktion über Args[]-Parameter
function demo3 
{
   write-host $args.count
   return $args[0] * $args[1]
 
}


#Ausführung (mit Debug-Einzelschritt...)
demo0                           #Keine Parameter!

demo1 5 "Demo 1 "               #Reihenfolge stimmt
demo1 "demo 1 " 5               #Reihenfolge falsch -> Error
demo1 -text "demo 1 " -zahl 5   #Parameter benannt

demo2 5 "Demo 2 "               #Reihenfolge stimmt
demo2 "Demo 2 " 5               #Reihenfolge falsch -> Error
demo2 -text "Demo 2 " -zahl 5   #Parameter benannt

demo3 "Demo 3 " 5               #Reihenfolge stimmt
demo3  5 "Demo 3 "              #Reihenfolge falsch -> Error
