﻿#Testet ob in Stringvar eine Zahl ist
function isNumeric ($x) {
    try {
        0 +$x | Out-Null  #Versucht zu rechnen mit der Variable
        return $true       #Wenn möglich: $true zurückgeben
    }
    catch {
        return $false      #Wenn Error: Ist keine Zahl --> $false
    }
}

function ReadIntRange ([string]$strTitel, [int]$untereGrenze, [int]$obereGrenze){

	[int] $intEingabe = 0;    # Variable für den Eingabewert (Achtung: Bereichsüberlauf wird nicht erkannt)
	[string]$strEingabe = ""; # für Stringeingabe
	[bool] $ok = $true;       # Fehlerflag

	do { # Text mit Bereichsgrenzen ausgeben:
        $strEingabe = read-Host " $strTitel (Bereich: $untereGrenze bis $obereGrenze)"
		$ok = $true;                        # Stringeingabe & Flagreset
        if (isNumeric($strEingabe)) {
		    $intEingabe = [int]$strEingabe; # wandelt in Int
		   
		    if (($intEingabe -gt $obereGrenze ) -or ($intEingabe -lt $untereGrenze)) { # Bereichstest
			    $ok = $false;               # Fehler markieren und Meldung ausgeben
			    write-host "`n Fehler! Eingabe ausserhalb Bereich! Bitte nochmals eingeben... `n"
            }
        } else {  
            $ok = $false;                   # Fehler markieren und Meldung ausgeben
			 write-host "`n Fehler! Eingabe muss nummerisch sein! Bitte nochmals eingeben... `n"
        }
	} while (!$ok)                          # wiederhole bis kein Fehler mehr ist!

	return $intEingabe;                     # Korrekte Eingabe zurückgeben
}


##################
# Hauptprogramm: #
##################
[int]$intZahl;   # Integerzahl

# Eingabetest:
write-host("Demo robuste INT-Eingabe`n========================`n(KEL, Version 0.2)`n");
[int]$intA = ReadIntRange "`nBitte eine ganze Zahl A eingeben" -2e6 +2e6 # Aufruf mit Text & Bereich
[int]$intB = ReadIntRange "`nBitte eine ganze Zahl B eingeben" -2e6 +2e6 # Aufruf mit Text & Bereich

if ($intB -ne 0) {
    [sng]$sngRes = $intA / $intB
    #Resultatausgabe;
    write-host "`nDas Resultat von  A/B ist <$sngRes>!`n"
} else {
    write-host "`nDas Resultat von  A/0 ist nicht berechenbar!`n"
}
