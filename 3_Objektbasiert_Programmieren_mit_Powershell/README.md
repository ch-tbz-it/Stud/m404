![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m404 Picto](../x_gitressourcen/M404_picto.jpg)


[TOC]

# M404 - Objektbasiert programmieren mit Windows Powershell

---

# Ablagestruktur
> Siehe auch Links im M404-Miroboard


[Workshop zum Einstieg G1, G2, ... -->> Hausaufgabe bis Seite 30 ](./00_WPS_Workshop_(G1_G2_...)) 

1. [WPS Unterlagen mit allen Kapiteln (ausser Testen)](./01_Unterlagen)
2. [INPUT Piping und Aliasing G1](./02_Input_Piping_Aliasing_(G1))
3. [INPUT Objekte G1](./03_Input_Objekte_(O1_G1))
4. [INPUT Ordner und Dateien O1 G2](./04_Input_Ordner_und_Dateien_(O1_G2))
5. [INPUT Abstraktion O3 P3](./05_Input_Abstraktion_(O3_P3))
6. [INPUT Robuste Eingabe & Menü O2 P2](./06_Input_Robuste_Eingabe_&_Menue_(O2_P2))
7. [INPUT Testen T2 T3](./07_Input_Testen_(T2_T3))
8. [Aufgabenstellungen O3 P3](./08_Aufgabenstellungen_(O3_P3))


## Lehrvideos ![Symbol](../x_gitressourcen/Video.png)

[MS Stream Kanal M404 Windows PowerShell](https://web.microsoftstream.com/channel/167b559f-a2cc-405b-855e-a50f2bd53fef)

> **Hinweis:** Erwähnungen und Referenzen in den Screencasts zum sog. "**BSCW**" sind nun auf **GitLAB**!
