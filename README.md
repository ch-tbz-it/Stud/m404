![TBZ Logo](./x_gitressourcen/tbz_logo.png)
![m404 Picto](./x_gitressourcen/M404_picto.jpg)


	
[TOC]

--

# M404 - Objektbasiert programmieren - SOL

1.Lehrjahr: Q3/Q4 für BI, nach BiVo 2014


## Modulbeschreibung:

Aufgrund einer Vorgabe Programmabläufe prozedural erstellen, testen und dokumentieren.


[Modul_ID und Kompetenzmatrix](./1_Administration/Modulidentifikation_M404.pdf)

---

# Ablagestruktur


1. [Administratives](./1_Administration)
2. [Selbstorganisiertes Lernen (SOL)](./2_SOL) >> Siehe Link im M404-Miroboard
3. [Programmieren mit Windows PowerShell](./3_Objektbasiert_Programmieren_mit_Powershell)


## Tools, um in PDFs (Unterlagen) schreiben zu können
- [Adobe Reader (Mac, WIN, Linux)
](https://www.adobe.com/ch_de/acrobat/pdf-reader.html?mv=search&sdid=X2PHHWM8&ef_id=EAIaIQobChMI_9S3ld6r-wIVRrrVCh27KAp-EAAYASAAEgIgQfD_BwE:G:s&s_kwcid=AL!3085!3!615802859657!e!!g!!adobe%20reader%20download!14655293243!126867447283&gclid=EAIaIQobChMI_9S3ld6r-wIVRrrVCh27KAp-EAAYASAAEgIgQfD_BwE)
- [PDF-Xchange Viewer (WIN)](https://www.chip.de/downloads/PDF-XChange-Viewer_29539244.html)
- [Vorschau (Mac)](https://setapp.com/de/so-gehts/pdf-dateien-bearbeiten-auf-mac?ci=13812291280&adgroupid=128678253040&adpos=&ck=&targetid=dsa-1408571174353&match=&gnetwork=g&creative=540413961743&placement=&placecat=&accname=setapp&gclid=EAIaIQobChMIjP2Jsd6r-wIVVvlRCh3dAgwqEAAYASAAEgKKK_D_BwE)


## Zugang Miroboard

![Miro Board](./x_gitressourcen/Miro.png)

1. Studenten-Account [erstellen](https://miro.com/education-whiteboard/) mit **TBZ Email-Adresse**!
2. Eigenes Miroboard erstellen (First board) und [Tutorial](https://www.youtube.com/watch?v=7L1-0DOGHDY) schauen.
3. Einführung [Lern-Portfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio) ( >> Siehe Link im M404-Miroboard)

## Lehrvideos ![Symbol](./x_gitressourcen/Video.png)

[MS Stream Kanal M404 Windows PowerShell](https://web.microsoftstream.com/channel/167b559f-a2cc-405b-855e-a50f2bd53fef)

> **Hinweis:** Erwähnungen und Referenzen in den Screencasts zum sog. "**BSCW**" sind nun auf **GitLAB**!
